$(document).on('ready', function(){

		MyGlobal.addMarkers = function (places, placesService, map){
			var me = this;
			this.places = places;
			this.placesService = placesService;
			this.map = map;

			var p1 = new Promise(function(resolve, reject){
				for (var j = 0; j < me.places.length; j++){
					console.log(me.places.length);

					(function(j) {
						setTimeout(function() {

							me.placesService.getDetails({
								placeId: me.places[j].place_id
							}, function(result, status){
								console.log(status);
								console.log(result);

								if(status == google.maps.places.PlacesServiceStatus.OK){
									var marker = new google.maps.Marker({
										map: me.map,
										place: {
											placeId: result.place_id,
											location: result.geometry.location
										}
									})
								}
							})

							if(me.places.length - 1 === j){
								resolve();
							}

						}, 500 * j);


					}(j));
				};

			})
			return p1;
		}
})
