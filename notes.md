Does it make sense to only declare variables in the scope where they are required?
* I declared variables in the initMap scope because I didn't know all the places where I may need them.


How can I apply OOP to my code so far?
In what ways can I make my code more scalable?

objects:
* map
* tour
* route
* marker
* routebox


* tips for improving skills?
* references?
* looking for a job right now, any advice? Preparing for interview etc.

# Underscore.JS
* widely used and very useful

# Binding and Scope
## avoid using var me = this;

# Pagination
* limit the amount of results from the api.
* show the user how many results have come back.
* add a more button, or pagination button module
	* click the next number in the module will issue a new request, for result x-y.

# Caching
* the user makes a request for a certain search, grab the results.
* if they make the same request - return the same results from the cache
* check the session
	* check session store by search s
	* there is a key
	* key for session is the string
	* check session to see if there is a match for the string
	* if nothing in the key for the session, then execute a new search.
* what are the limits?
	* how many results can be saved in the browser?
* Should
* is this valuable or not on the front end?

# Testing
* jasmine
*
